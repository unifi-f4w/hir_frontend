#!/bin/sh

set -xe

source ./config.sh

IMAGE=quay.io/hartliebevo/f4w-hir-web:${IMAGE_VERSION}


[ -z "$CI_BUILD_TOKEN" ] || docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} registry.gitlab.com
docker pull ${IMAGE}
docker rm source || true
docker create --name source ${IMAGE}
rm -rf htdocs
docker cp source:/usr/local/apache2/htdocs/ ./htdocs
docker rm source || true
docker build -t registry.gitlab.com/unifi-f4w/hir_frontend/nginx:${IMAGE_VERSION} .
[ -z "$PUSH_IMAGE"] || docker push registry.gitlab.com/unifi-f4w/hir_frontend/nginx:${IMAGE_VERSION}
